<?php

namespace App;

use MailWizzApi_Config;

final class Mailwizz {
  public $endpoint;

  public function __construct() {
    $config = new MailWizzApi_Config(array(
      'apiUrl'        => 'http://hvm.pe/wizz/api/index.php',
      'publicKey'     => 'be688a788bf4c625c1bd9411bc491f683003d271',
      'privateKey'    => '317d40b262bcc9af3a3d4a2c8e2225df855b95be'
    ));

    \MailWizzApi_Base::setConfig($config);

    date_default_timezone_set('UTC');

    $this->endpoint = new \MailWizzApi_Endpoint_Lists();
  }
}
